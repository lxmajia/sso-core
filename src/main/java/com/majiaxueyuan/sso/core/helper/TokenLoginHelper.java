package com.majiaxueyuan.sso.core.helper;

import com.majiaxueyuan.sso.core.constans.Result;
import com.majiaxueyuan.sso.core.constans.SysCfg;
import com.majiaxueyuan.sso.core.entity.SSOUser;
import com.majiaxueyuan.sso.core.util.JwtTokenUtils;

// While Login Used to Create Token
public class TokenLoginHelper {

	public static Result loginSuccess(Long userId, String username, String otherParam, String tokenSalt) {
		if (tokenSalt != null && !tokenSalt.equals("")) {
			if (!SysCfg.TOKEN_SALT.equals(tokenSalt)) {
				SysCfg.TOKEN_SALT = tokenSalt;
			}
		}
		// Check UserId isNull
		if (userId == null) {
			return Result.failed("认证唯一性不能为空");
		}
		// pass check
		SSOUser user = new SSOUser();
		user.setId(userId);
		user.setUsername(username);
		user.setOther(otherParam);
		// success to create token
		String token = JwtTokenUtils.createToken(user);
		return Result.success(token);
	}
}
